#include<iostream>
#include<string>

using namespace std;

class Odometer {
	public:
	string digits = "123456789";
	int START;
	int END;
	int size;

	public: Odometer(int n) {
		START = stoi(digits.substr(0,n));
		END = stoi(digits.substr(9-n,n));
	}

	bool isAscending(int reading) {
		if (reading < 10)
			return true;
		if (reading % 10 <= (reading / 10) % 10)
			return false;
		reading /= 10;
		return isAscending(reading);
	}

	int nextReading(int reading) {
		reading++;
		if (reading > END)
			return START;
		if (isAscending(reading))
			return reading;
		return nextReading(reading);
	}

	int prevReading(int reading) {
		reading--;
		if (reading < START)
			return END;
		if (isAscending(reading))
			return reading;
		return prevReading(reading);
	}
};

int main(int argc, char const *argv[])
{
	Odometer o(4);
	cout << o.prevReading(1234);
	return 0;
}